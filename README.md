# IFrame Events

Manejo de eventos de API Browser Message para comunicación entre shell y micro-frontends.

Su principal objetivo es transformar eventos recibidos por API de mensajes (postMessage) y transformarlos
en Subjects de rxjs para mejor manejo en frameworks.

# Subir versión

Asumiendo que la versión a publicar es la `1.0.2` según el [versionado semántico](https://semver.org):

Luego de hacer las modificaciones para la nueva versión

1. Ejecutar `npm version 1.0.2`. El script preversion se encargará de buildear el proyecto y el script postversion de crear el tag y pushear master y el nuevo tag.
2. Ejecutar el Pipeline manual para publicación ([listado de jobs](https://gitlab.com/treserres/librerias/iframe-events/-/jobs)).
