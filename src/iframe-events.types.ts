export interface BaseIFrameEvent {
  type: IFrameEventType;
};

export interface NavigationIFrameEvent extends BaseIFrameEvent {
  type: IFrameEventType.navigation;
  sistema?: string;
  url: string;
}

export interface UrlStateIFrameEvent extends BaseIFrameEvent {
  type: IFrameEventType.urlState;
  sistema?: string;
  url: string;
}

export interface SignInIFrameEvent extends BaseIFrameEvent {
  type: IFrameEventType.signIn;
  accessToken: string;
}

export interface SignOutIFrameEvent extends BaseIFrameEvent {
  type: IFrameEventType.signOut;
}

export interface LanguageIFrameEvent extends BaseIFrameEvent {
  type: IFrameEventType.language;
  lang: string;
}

export interface NotificationIFrameEvent extends BaseIFrameEvent {
  type: IFrameEventType.notification;
  title: string;
  message: string;
}

export type IFrameEvent = BaseIFrameEvent | NavigationIFrameEvent | UrlStateIFrameEvent | SignInIFrameEvent
  | SignOutIFrameEvent | LanguageIFrameEvent | NotificationIFrameEvent;

export enum IFrameEventType {
  navigation = 'navigation',
  urlState = 'url-state',
  signIn = 'sign-in',
  signOut = 'sign-out',
  language = 'language',
  notification = 'notification'
}

export interface UrlAmbientes {
  [sistema: string]: {
    frontend?: string;
    api?: string
  }
}