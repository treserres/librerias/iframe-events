import { Subject } from 'rxjs';
import { IFrameEvent, IFrameEventType, LanguageIFrameEvent, NavigationIFrameEvent, NotificationIFrameEvent, SignInIFrameEvent, UrlAmbientes, UrlStateIFrameEvent } from './iframe-events.types';

/**
 * Manejo de eventos PostMessage para comunicación entre shell y micro-frontends.
 *
 * Su principal objetivo es transformar eventos recibidos por API de mensajes (postMessage) y transformarlos
 * en Subjects de rxjs para mejor manejo en frameworks.
 */
export class IframeEvents {

  /**
   * Elemento HTML que representa al iframe, utilizado para emitir eventos hacia micro-frontend
   */
  iframe: HTMLIFrameElement | null = null;

  /**
   * Si se especifica algo diferente a '*', restringe el envío de eventos a ese destino
   */
  origin: string = '*';

  /**
   * Evento que se emite cuando se debe actualizar el token actual
   */
  newAccessToken = new Subject<string>();

  /**
   * Evento que se emite cuando se debe navegar a la URL especificada
   */
  routeNavigation = new Subject<string>();

  /**
   * Evento que se emite cuando se debe actualizar la URL actual por cambio de parámetros
   */
  replaceState = new Subject<string>();

  /**
   * Evento que se emite cuando se debe actualizar el idioma establecido para internacionalización
   */
  languageChange = new Subject<string>();

  /**
   * Evento que se emite cuando el micro-frontend quiere notificar algo via shell
   */
  showNotification = new Subject<{ title: string, message: string }>();

  /**
   * Configuración del iframe para que realice navegación interna utilizando el router.
   *
   * Scope: shell
   */
  configurarEventosShell(urls: UrlAmbientes, origin: string = '*'): void {
    window.onmessage = (event: MessageEvent<IFrameEvent>): void => {
      if (event.data?.type === IFrameEventType.navigation) {
        const data = event.data as NavigationIFrameEvent;
        if (data.sistema && urls?.[data.sistema]?.frontend) {
          this.routeNavigation.next(`/${data.sistema}${data.url}`);
        }
      } else if (event.data?.type === IFrameEventType.urlState) {
        const data = event.data as UrlStateIFrameEvent;
        if (data.sistema && urls?.[data.sistema]?.frontend) {
          this.replaceState.next(`/${data.sistema}${data.url}`);
        }
      } else if (event.data?.type === IFrameEventType.signIn) {
        const data = event.data as SignInIFrameEvent;
        this.newAccessToken.next(data.accessToken);
        this.routeNavigation.next('/');
      } else if (event.data?.type === IFrameEventType.signOut) {
        this.routeNavigation.next('/sso/sign-out');
      } else if (event.data?.type === IFrameEventType.language) {
        const data = event.data as LanguageIFrameEvent;
        this.languageChange.next(data.lang);
      } else if (event.data?.type === IFrameEventType.notification) {
        const { title, message } = event.data as NotificationIFrameEvent;
        this.showNotification.next({ title, message });
      }
    };

    this.origin = origin;
  }

  /**
   * Registra el iframe para poder emitir eventos desde este servicio
   *
   * Scope: shell
   *
   * @param iframe
   */
  registrarIFrame(iframe: HTMLIFrameElement): void {
    this.iframe = iframe;
  }

  /**
   * Configuración del iframe para que realice navegación interna utilizando el router.
   *
   * Scope: micro-frontend
   */
  configurarEventosMicroFrontend(): void {
    window.onmessage = (event: MessageEvent<IFrameEvent>): void => {
      if (event.data?.type === IFrameEventType.navigation) {
        const data = event.data as NavigationIFrameEvent;
        this.routeNavigation.next(data.url);
      } else if (event.data?.type === IFrameEventType.language) {
        const data = event.data as LanguageIFrameEvent;
        localStorage.setItem('language', data.lang);
        this.languageChange.next(data.lang);
      }
    };

    // Setea token pasado por iframe si existe
    const url = new URL(window.location.href);
    const token = url.searchParams.get('token');
    if (token) {
      this.newAccessToken.next(token);
    }
  }

  /**
   * Emite evento de navegación interna hacia el micro-frontend para navegue mediante router.
   *
   * Scope: shell
   *
   * @param url
   */
  emitirNavegacionInterna(url: string): void {
    this.iframe?.contentWindow?.postMessage({
      type: IFrameEventType.navigation,
      url
    }, this.origin);
  }

  /**
   * Emite evento de navegación externa para que para shell navegue.
   *
   * Scope: micro-frontend
   *
   * @param url
   * @param sistema
   */
  emitirNavegacionExterna(url: string, sistema: string): void {
    window.top?.postMessage({
      type: IFrameEventType.navigation,
      url,
      sistema
    }, this.origin);
  }

  /**
   * Emite evento de logueo de usuario desde micro-frontend a shell
   *
   * Scope: micro-frontend
   *
   * @param accessToken
   */
  emitirEventoSignIn(accessToken: string): void {
    window.top?.postMessage({
      type: IFrameEventType.signIn,
      accessToken
    }, this.origin);
  }

  /**
   * Emite evento de cierre de sesión desde micro-frontend a shell
   *
   * Scope: micro-frontend
   */
  emitirEventoSignOut(): void {
    window.top?.postMessage({
      type: IFrameEventType.signOut
    }, this.origin);
  }

  /**
   * Emite evento de cambio de lenguaje, puede prevenir de shell o micro-frontend
   *
   * Scope: shell y micro-frontend
   *
   * @param lang
   */
  emitirEventoLenguaje(lang: string): void {
    (this.iframe ? this.iframe.contentWindow : window.top)?.postMessage({
      type: IFrameEventType.language,
      lang
    }, this.origin);
  }

  /**
   * Emite evento de actualización de state, útil para que shell actualice su URL cuando cambian parámetros en ruta
   *
   * Scope: micro-frontend
   *
   * @param url
   * @param sistema
   */
  emitirUrlState(url: string, sistema: string): void {
    window.top?.postMessage({
      type: IFrameEventType.urlState,
      url,
      sistema
    }, this.origin);
  }

  /**
   * Emite evento para notificar a usuario.
   * Útil cuando el sistema redirige a otro y se necesita que el feedback lo de el shell.
   *
   * Scope: micro-frontend
   *
   * @param title
   * @param message
   */
  emitirNotification(title: string, message: string): void {
    window.top?.postMessage({
      type: IFrameEventType.urlState,
      title,
      message
    }, this.origin);
  }
}
